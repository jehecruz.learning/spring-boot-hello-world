# spring-boot-hello-world

Este microservicio tiene 1 API que se encargan de realizar lo siguiente:

1. Mostrar el nombre del tema visto en clase.  ***GET**


## RECURSO
> /api/v1

URL :

http://localhost:8080/api/v1


### Tools

* Maven
* Spring
* SpringBoot
* Spring Tools Suite


### Prerequisitos

Se necesita tener instalado:
		
- Java 1.8  		
- Maven 		
- Spring Tools Suite

 
### Deploy
    mvn spring boot:run  en Local 
    O desde Spring Tools Suite click derecho sobre el proyecto -> Run As -> Spring Boot App 
